var datacount = localStorage.length;
if (datacount > 0) {
	var render = "<table border='1'>";
	render += "<tr><th>Name</th><th>Description</th><th>Place</th><th>Date & Time</th><th>Event</th><th>Edit</th><th>Delete</th></tr>";
	for (i = 0; i < datacount; i++) {
			var key = localStorage.key(i); //Get  the Key 
			var person = localStorage.getItem(key); //Get Data from Key 
			var data = JSON.parse(person); // var data = JSON.parse(myArray); this is wrong
			render += "<tr><td>" + data[0] + " </td>"; //data.name is wrong 
			render += "<td>" +  data[1]  + "</td>"; //desc
			render += "<td>" +  data[2]  + "</td>"; //place
			render += "<td>" +  data[3]  + "</td>"; //date & time
			render += "<td>" +  data[4]  + "</td>"; //event
			render += "<td>" + "<a href=\'edit.html?key="+key+"\' >Edit</a>" + "</td>";
			render += "<td>" + "<a href=\'delete.html?key="+key+"\' >Delete</a>" + "</td></tr>";

		  }
        render += "</table>";
        document.write(render); //here
    }
    else {
        document.write("<div class='NoSaved'>No saved events</div>");
        //class='error' ==> give this class some CSS effects 
    }