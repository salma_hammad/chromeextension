function click(e) {
    if(e.target.id =="newBtn"){
        window.location.href="new_event.html";
    }
    else if(e.target.id =="displayBtn"){
        window.location.href="display_all.html";
    }
	else if(e.target.id =="backBtn") {
		window.location.href="popup.html";
	}
	
}

document.addEventListener('DOMContentLoaded', function () {
  var inputs = document.querySelectorAll('input');
  for (var i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('click', click);
  }
});